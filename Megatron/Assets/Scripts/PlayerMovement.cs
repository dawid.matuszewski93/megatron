﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float _xMovement;
    private float _zMovement;
    private CharacterController _charController;
    private float _speed = 5f;

    [SerializeField] private Transform _groundChecker;
    private float _groundDistance = 0.4f;
    public LayerMask groundMask;
    private bool _isGrounded;

    private Vector3 _velocity;
    private float _gravity = -9.8f;

    void Awake()
    {
        _charController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = Physics.CheckSphere(_groundChecker.position, _groundDistance, groundMask);

        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = -2f;
        }

        _xMovement = Input.GetAxis("Horizontal");
        _zMovement = Input.GetAxis("Vertical");

        Vector3 Move = transform.right * _xMovement + transform.forward * _zMovement;
        _charController.Move(Move * _speed * Time.deltaTime);

        _velocity.y += _gravity * Time.deltaTime;
        _charController.Move(_velocity * Time.deltaTime);
        

    }
}
