﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

//skrypt odpowiedzialny za obsługę Audio. Do prawidłowej pracy potrzebuje klasę Sound.
public sealed class SoundPlayer : MonoBehaviour {
    public static SoundPlayer Instance { get; private set; }

    public Sound[] sounds;

    //Biblioteka nazw dźwięków.  
    public readonly string theme1 = "Theme1";  
    public readonly string Lol = "Lol";

    private void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.audioMixer;
        }
    }
    public void PlayTest()
    {
        Play(theme1);
    }
    public void Play(string p_name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == p_name);
        if (s == null)
        {
            return;
        }
        s.source.Play();
    }

    public void Stop(string p_name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == p_name);
        if (s == null)
        {
            return;
        }
        s.source.Stop();
    }

    public void UnPause(string p_name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == p_name);
        if (s == null)
        {
            return;
        }
        s.source.UnPause();       
    }

    public void Pause(string p_name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == p_name);
        if (s == null)
        {
            return;
        }
        s.source.Pause();
    }

    public void PlayOne(string p_name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == p_name);
        if (s == null)
        {
            return;
        }
        AudioClip audio = s.clip;
        s.source.PlayOneShot(audio, 1f);
    }

    public IEnumerator PlayDelayedSound(float _timeInSeconds, string _soundName)
    {
        yield return new WaitForSeconds(_timeInSeconds);
        Play(_soundName);
    }
}
